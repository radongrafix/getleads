json.extract! facebook_page, :id, :page_name, :username, :page_email, :page_address, :page_about, :page_mission, :page_url, :page_profile_image, :user_id, :created_at, :updated_at
json.url facebook_page_url(facebook_page, format: :json)
