module ApplicationHelper
	def app_name
		app_name = "Gleadr"
	end

	def default_fb_image
		img = asset_url("gleadr.png")
	end

	def login_photo
		login_photo = asset_path("login.jpg")
	end

	def signup_photo
		login_photo = asset_path("signup.jpg")
	end

	def remember_photo
		login_photo = asset_path("remember.jpg")
	end
	def forgot_photo
		login_photo = asset_path("forgot_password.jpg")
	end


	def my_name
		user = User.find(current_user.id)
		if user.profile.first_name.nil? && user.profile.last_name.nil?
			full_name = "Your Name"
		else
			full_name = user.profile.first_name.capitalize.to_s + " "  + user.profile.last_name.capitalize.to_s
		end
	end

	def year
		year = Time.new.year
	end
	def flash_class(level)
	    case level
	        when 'notice' then "alert alert-dismissable alert-info"
	        when 'success' then "alert alert-dismissable alert-success"
	        when 'error' then "alert alert-dismissable alert-danger"
	        when 'alert' then "alert alert-dismissable alert-danger"
	    end
	end

	ALERT_TYPES = [:success, :info, :warning, :danger] unless const_defined?(:ALERT_TYPES)

	def bootstrap_flash(options = {})
	    flash_messages = []
	    flash.each do |type, message|
	      # Skip empty messages, e.g. for devise messages set to nothing in a locale file.
	      next if message.blank?

	      type = type.to_sym
	      type = :success if type == :notice
	      type = :danger  if type == :alert
	      type = :danger  if type == :error
	      next unless ALERT_TYPES.include?(type)

	      tag_class = options.extract!(:class)[:class]
	      tag_options = {
	        class: "alert alert-#{type} #{tag_class}",
	        	id: "success-alert"
	      }.merge(options)

	      close_button = content_tag(:button, raw("&times;"), type: "button", class: "close", "data-dismiss" => "alert")

	      Array(message).each do |msg|
	        text = content_tag(:div, close_button + msg, tag_options)
	        flash_messages << text if msg
	      end
	    end
	    flash_messages.join("\n").html_safe
	end

end
