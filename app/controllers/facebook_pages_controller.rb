require 'open-uri'
require 'nokogiri'

class FacebookPagesController < ApplicationController
  before_action :set_facebook_page, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /facebook_pages
  # GET /facebook_pages.json
  def index
    @facebook_pages = FacebookPage.where(user_id: current_user).all
    @fb_email_only = FacebookPage.where.not(page_email: [nil, ""]).where(page_phone: [nil, ""]).where(user_id: current_user)
    @fb_phone_only = FacebookPage.where.not(page_phone: [nil, ""]).where(page_email: [nil, ""]).where(user_id: current_user)
  end

  def bulk_fb_page
    # @page_name = []
    # @url_page = []
    

    max_page_count = params[:max_page_count]

    max_page_count.to_i.times do |i|
      # @page_total = @page_name.size
      category = params[:category_slugs] #'personal-trainer'

      starting_page = params[:starting_page]
      page_num = starting_page.to_i
      
      url = "https://www.facebook.com/pages/category/#{category}/?page=#{i+page_num}"


      page = Nokogiri::HTML(open(url))

      page.xpath('//comment()').each { |comment| comment.replace(comment.text) }

      code = page.serialize

      @html_doc = Nokogiri::HTML(code)



      # html_doc.css("a._6x0d").each do |item|
      #   #url_page << item["href"]

      #   fb_url = item["href"]
      #   @url_page = fb_url+ "#{'about'}"

      #   fb_page = Nokogiri::HTML(open(@url_page))

      #   @page_title = fb_page.at("meta[property='og:title']")['content'] 

      #   # @page_count = @page_title.size

      #   @page_phone = fb_page.css("#content_container ._50f4").text[/([+|\s]\d+\s\d+[-|\s]\d+[-|\s]\d+|\d+\s\d+\s\d+|\(?\d+\)+\s\d+\s\d+|[\d]{2,}\s[\d]{3,4}[\-]\d+|[\d]{2,3}[\-][\d]{3,4}[\-][\d]{3,4})/i]
      #   @page_email = fb_page.css('a[href^="mailto:"]').text.strip

      
        

      # end


    end
       

  end

  # GET /facebook_pages/1
  # GET /facebook_pages/1.json
  def show
  end

  # GET /facebook_pages/new
  def new
    @facebook_page = FacebookPage.new
  end

  # GET /facebook_pages/1/edit
  def edit
  end

  # POST /facebook_pages
  # POST /facebook_pages.json
  def create
    @facebook_page = FacebookPage.new(facebook_page_params)
    @facebook_page.user_id = current_user.id
    respond_to do |format|
      if @facebook_page.save
        format.html { redirect_to @facebook_page, notice: 'Facebook page was successfully created.' }
        format.json { render :show, status: :created, location: @facebook_page }
      else
        format.html { render :new }
        format.json { render json: @facebook_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /facebook_pages/1
  # PATCH/PUT /facebook_pages/1.json
  def update
    respond_to do |format|
      if @facebook_page.update(facebook_page_params)
        format.html { redirect_to @facebook_page, notice: 'Facebook page was successfully updated.' }
        format.json { render :show, status: :ok, location: @facebook_page }
      else
        format.html { render :edit }
        format.json { render json: @facebook_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /facebook_pages/1
  # DELETE /facebook_pages/1.json
  def destroy
    @facebook_page.destroy
    respond_to do |format|
      format.html { redirect_to facebook_pages_url, notice: 'Facebook page was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_facebook_page
      @facebook_page = FacebookPage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def facebook_page_params
      params.require(:facebook_page).permit(:page_name, :username,:page_phone, :page_email, :page_address, :page_about, :page_mission, :page_url, :page_profile_image, :user_id)
    end
end
