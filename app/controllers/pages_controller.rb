class PagesController < ApplicationController
  def index
  end

  def dashboard
  	@facebook_count = FacebookPage.where(user_id: current_user).count
  	@fb_email_only_count_total = FacebookPage.where.not(page_email: [nil, ""]).where(page_phone: [nil, ""]).where(user_id: current_user).count
  	@fb_phone_only_count_total = FacebookPage.where.not(page_phone: [nil, ""]).where(page_email: [nil, ""]).where(user_id: current_user).count

  	@fb_email_phone_count = FacebookPage.where.not(page_email: [nil, ""], page_phone: [nil, ""]).where(user_id: current_user).count
  end
end
