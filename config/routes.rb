Rails.application.routes.draw do
  resources :facebook_pages
  get 'bulk-page', to: 'facebook_pages#bulk_fb_page'
	# resources :profiles
    # post 'profiles/create'
    # get 'profiles/update'
    put '/profiles/:id' => 'profiles#update', as: :profile
    get '/profiles/:id/edit' =>  'profiles#edit', as: :edit_profile
    put '/profiles/:id/edit' =>  'profiles#edit'  
    patch  '/profiles/:id(.:format)' => 'profiles#update'
	devise_for :users
	devise_scope :user do
		authenticated :user do
		  root to: "pages#dashboard", as: :authenticated_root
		end

		unauthenticated do
		  root 'pages#index', as: :unauthenticated_root
		end
	end


	get '*path' => redirect('/404.html')
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
