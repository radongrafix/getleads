class CreateFacebookPages < ActiveRecord::Migration[5.2]
  def change
    create_table :facebook_pages do |t|
      t.string :page_name
      t.string :username
      t.string :page_email
      t.text :page_address
      t.text :page_about
      t.text :page_mission
      t.string :page_url
      t.string :page_profile_image
      t.belongs_to :user, foreign_key: true

      t.timestamps
    end
  end
end
