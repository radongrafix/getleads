class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
    	t.string :avatar
      	t.string :first_name
      	t.string :last_name
      	t.text :address
      	t.belongs_to :user, foreign_key: true

      	t.timestamps
    end
  end
end
