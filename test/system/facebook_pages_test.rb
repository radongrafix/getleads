require "application_system_test_case"

class FacebookPagesTest < ApplicationSystemTestCase
  setup do
    @facebook_page = facebook_pages(:one)
  end

  test "visiting the index" do
    visit facebook_pages_url
    assert_selector "h1", text: "Facebook Pages"
  end

  test "creating a Facebook page" do
    visit facebook_pages_url
    click_on "New Facebook Page"

    fill_in "Page about", with: @facebook_page.page_about
    fill_in "Page address", with: @facebook_page.page_address
    fill_in "Page email", with: @facebook_page.page_email
    fill_in "Page mission", with: @facebook_page.page_mission
    fill_in "Page name", with: @facebook_page.page_name
    fill_in "Page profile image", with: @facebook_page.page_profile_image
    fill_in "Page url", with: @facebook_page.page_url
    fill_in "User", with: @facebook_page.user_id
    fill_in "Username", with: @facebook_page.username
    click_on "Create Facebook page"

    assert_text "Facebook page was successfully created"
    click_on "Back"
  end

  test "updating a Facebook page" do
    visit facebook_pages_url
    click_on "Edit", match: :first

    fill_in "Page about", with: @facebook_page.page_about
    fill_in "Page address", with: @facebook_page.page_address
    fill_in "Page email", with: @facebook_page.page_email
    fill_in "Page mission", with: @facebook_page.page_mission
    fill_in "Page name", with: @facebook_page.page_name
    fill_in "Page profile image", with: @facebook_page.page_profile_image
    fill_in "Page url", with: @facebook_page.page_url
    fill_in "User", with: @facebook_page.user_id
    fill_in "Username", with: @facebook_page.username
    click_on "Update Facebook page"

    assert_text "Facebook page was successfully updated"
    click_on "Back"
  end

  test "destroying a Facebook page" do
    visit facebook_pages_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Facebook page was successfully destroyed"
  end
end
