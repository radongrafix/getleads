require 'test_helper'

class FacebookPagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @facebook_page = facebook_pages(:one)
  end

  test "should get index" do
    get facebook_pages_url
    assert_response :success
  end

  test "should get new" do
    get new_facebook_page_url
    assert_response :success
  end

  test "should create facebook_page" do
    assert_difference('FacebookPage.count') do
      post facebook_pages_url, params: { facebook_page: { page_about: @facebook_page.page_about, page_address: @facebook_page.page_address, page_email: @facebook_page.page_email, page_mission: @facebook_page.page_mission, page_name: @facebook_page.page_name, page_profile_image: @facebook_page.page_profile_image, page_url: @facebook_page.page_url, user_id: @facebook_page.user_id, username: @facebook_page.username } }
    end

    assert_redirected_to facebook_page_url(FacebookPage.last)
  end

  test "should show facebook_page" do
    get facebook_page_url(@facebook_page)
    assert_response :success
  end

  test "should get edit" do
    get edit_facebook_page_url(@facebook_page)
    assert_response :success
  end

  test "should update facebook_page" do
    patch facebook_page_url(@facebook_page), params: { facebook_page: { page_about: @facebook_page.page_about, page_address: @facebook_page.page_address, page_email: @facebook_page.page_email, page_mission: @facebook_page.page_mission, page_name: @facebook_page.page_name, page_profile_image: @facebook_page.page_profile_image, page_url: @facebook_page.page_url, user_id: @facebook_page.user_id, username: @facebook_page.username } }
    assert_redirected_to facebook_page_url(@facebook_page)
  end

  test "should destroy facebook_page" do
    assert_difference('FacebookPage.count', -1) do
      delete facebook_page_url(@facebook_page)
    end

    assert_redirected_to facebook_pages_url
  end
end
